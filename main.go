package main

import (
	"fmt"
	"time"
)

func main() {
	messages := make(chan int, 3) //try to change value

	go func() {
		for {
			i := <-messages
			fmt.Println("receive data", i)
		}
	}()

	for i := 0; i < 5; i++ {
		fmt.Println("send data", i)
		messages <- i
	}
	time.Sleep(time.Millisecond)
}
